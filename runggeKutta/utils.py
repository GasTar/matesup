calculateH = lambda xinicial, xfinal, pasos: (xfinal - xinicial) / pasos
calculatePasos = lambda xinicial, xfinal, h: int((xfinal - xinicial) // h)

def euler(puntoI, h, funciones):
    proximoX = puntoI[0] + h
    res = []
    res.append(proximoX)
    for i in range(len(funciones)):
        fn = funciones[i]
        res.append(puntoI[i+1] + h * fn(puntoI))
    return res


def eulerMejorado(puntoI, h, funciones):
    proximoX = puntoI[0] + h
    res = []

    pEuler = []
    pEuler.append(proximoX)
    
    res.append(proximoX)
    for i in range(len(funciones)):
        fn = funciones[i]
        pEuler.append(puntoI[i+1] + h * fn(puntoI))

    for i in range(len(funciones)):
        res.append(puntoI[i+1] + (h/2) * (funciones[i](puntoI) + funciones[i](pEuler)))

    return res

def rungekuttaCuarto(puntoI, h, funciones):
    proximoX = puntoI[0] + h
    res = []
    res.append(proximoX)

    pk1 = []
    pk2 = []
    pk3 = []
    pk4 = []

    pIntermediok2 = [puntoI[0] + (h/2)]
    pIntermediok3 = [puntoI[0] + (h/2)]
    pIntermediok4 = [puntoI[0] + h]

    for i in range(len(funciones)):
        fn = funciones[i]

        k1 = fn(puntoI)
        pk1.append(k1)

    for i in range(len(funciones)):
        pIntermediok2.append(puntoI[i+1] + (h/2) * pk1[i])

    for i in range(len(funciones)):
        fn = funciones[i]
        k2 = fn(pIntermediok2)
        pk2.append(k2)

    for i in range(len(funciones)):
        pIntermediok3.append(puntoI[i+1] + (h/2) * pk2[i])

    for i in range(len(funciones)):
        fn = funciones[i]
        k3 = fn(pIntermediok3)
        pk3.append(k3)

    for i in range(len(funciones)):
        pIntermediok4.append(puntoI[i+1] + (h) * pk3[i])

    for i in range(len(funciones)):
        fn = funciones[i]
        k4 = fn(pIntermediok4)
        pk4.append(k4)

    for i in range(len(funciones)):
        res.append(
            puntoI[i+1] +
            (h/6) * ( pk1[i] + 2 * pk2[i] + 2 * pk3[i] + pk4[i])
        )

    return res
